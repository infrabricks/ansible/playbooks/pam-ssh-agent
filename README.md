# PAM SSH agent

Ansible playbook to use SSH agent + PAM for sudo auth. 

## OS

* Debian

## Requirements

* Ansible >= 4.

## Role Variables

Variable name       | Description               | Required | Default
---                 | ---                       | ---      | ---
pam_packages        | Packages to install       | yes      | yes
pam_authorized_keys | Authorized keys file path | yes      | yes

## Dependencies

No.

## License

* GPL v3

## Author Information

* [Stéphane Paillet](mailto:spaillet@ethicsys.fr)

